package com.example.android_task_1_simple_list

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class MyRecyclerAdapter(var context: Context, private val vector_: MutableList<ElementParent>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var vector: MutableList<ElementParent> = vector_;

    @SuppressLint("ResourceType")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == 0){
            val rowView: View = LayoutInflater.from(parent.context).inflate(R.layout.parent_element, parent,false)
            GroupViewHolder(rowView)
        } else {
            val rowView: View = LayoutInflater.from(parent.context).inflate(R.layout.child_element, parent,false)
            ChildViewHolder(rowView)
        }
    }

    override fun getItemViewType(position: Int): Int = vector[position].type

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val el = vector[position]
        if (el.type == 0) { // parent
            holder as GroupViewHolder
            holder.apply {
                title.text = el.title
                button.setOnClickListener {
                    clickAction(el, position)
                }
            }
        } else { // child
            holder as ChildViewHolder

            holder.apply {
                commentsCount.text = "comments: ${el.child.commentsCount}"
                link.setOnClickListener {
                    val uri = Uri.parse("https://${el.child.domain}")
                    context.startActivity(Intent(Intent.ACTION_VIEW, uri))
                }
            }
        }
    }
    private fun clickAction(el: ElementParent, id: Int) {
        if (el.isExpanded) {
            collapse(el, id)
        } else {
            expand(el, id)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun expand(el: ElementParent, id: Int){
        vector[id].isExpanded = true
        if(el.type == 0) {
            vector.add(
                id+1,
                MyRecyclerAdapter.ElementParent(
                    el.title,
                    1,
                    MyRecyclerAdapter.ElementChild(
                        el.child.domain,
                        el.child.by,
                        el.child.age,
                        el.child.score,
                        el.child.commentsCount
                    ),
                    false
                )
            )
            notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun collapse(el: ElementParent, id: Int){
        vector[id].isExpanded = false
        if(vector[id].type == 0){
            vector.removeAt(id + 1)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int = vector.size

    class GroupViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val title = row.findViewById(R.id.parent_title) as TextView
        val button  = row.findViewById(R.id.parent_button) as ImageView
    }
    class ChildViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val commentsCount = row.findViewById(R.id.child_commentsCount) as TextView
        val link = row.findViewById(R.id.child_link) as ImageView
    }

    data class ElementParent(
        val title: String,
        var type:Int, // zero is parent, one - child
        var child : ElementChild,
        var isExpanded:Boolean
    )

    data class ElementChild(
        val domain: String,
        val by: String,
        val age: String,
        val score: Int,
        val commentsCount: Int
    )
}

class MainActivity : AppCompatActivity() {
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listData : MutableList<MyRecyclerAdapter.ElementParent> = ArrayList()
        // parsing elements to Parent/Child versions
        for(element in elements) {
            listData.add(
                MyRecyclerAdapter.ElementParent(
                    element.title,
                    0,
                    MyRecyclerAdapter.ElementChild(
                        element.domain,
                        element.by,
                        element.age,
                        element.score,
                        element.commentsCount
                    ),
                    false
                )
            )
        }

        val recyclerView: RecyclerView = findViewById(R.id.idRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = MyRecyclerAdapter(this, listData)

        val pullToRefresh = findViewById<SwipeRefreshLayout>(R.id.idRefresher)!!
        pullToRefresh.setOnRefreshListener {
            if ((recyclerView.adapter as MyRecyclerAdapter).itemCount > 0) (recyclerView.adapter as MyRecyclerAdapter).vector.removeAt(0)
            (recyclerView.adapter as MyRecyclerAdapter).notifyDataSetChanged()
            pullToRefresh.isRefreshing = false;
        }
    }
}
